# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from decimal import Decimal
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Paragraph, Table ,TableStyle ,SimpleDocTemplate,Image
from reportlab.lib.utils import simpleSplit,ImageReader
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT, TA_JUSTIFY
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle,getSampleStyleSheet
from reportlab.pdfgen import canvas
from odoo.addons.report_it.utils import register_fonts, get_encoded_image
import os

class SaleOrder(models.Model):
	_inherit='sale.order'

	def build_report_pdf(self):
		if not self: return
		if any(lt.partner_id.id != self[0].partner_id.id for lt in self):
			raise UserError('User error')

		# Formato estándard de letra
		path = self.env['bo.report.base'].get_reports_path()
		now = fields.Datetime.context_timestamp(self, fields.Datetime.now())
		file_name = u'Cotizacion %s.pdf' % str(now)[:19].replace(':','_')
		path += file_name
		register_fonts(['Calibri', 'Calibri-Bold'])
		wPage, hPage = A4
		pos_left = 10
		c = canvas.Canvas(path , pagesize= (wPage, hPage))
		wUtil = wPage - 2 * pos_left # Util width : Real Width - left margins
		middle = wPage / 2 # Center page
		pos = hPage - 150
		separator = 12
		bottom = 55
		col_widths = [float(i)/100*wUtil for i in (53,9.8,9.5,8,10.8,9)]

		p1 = ParagraphStyle('p1', alignment=TA_CENTER, fontSize=8, fontName="Calibri-Bold")
		p2 = ParagraphStyle('p2', alignment=TA_LEFT, fontSize=10, fontName="Calibri")
		p3 = ParagraphStyle('p3', alignment=TA_LEFT, fontSize=10, fontName="Calibri")
		p4 = ParagraphStyle('p4', alignment=TA_RIGHT, fontSize=10, fontName="Calibri")
		p5 = ParagraphStyle('p5', alignment=TA_RIGHT, fontSize=10, fontName="Calibri-Bold")
		p6 = ParagraphStyle('p6', alignment=TA_LEFT, fontSize=10, fontName="Calibri-Bold")
		p7 = ParagraphStyle('p7', alignment=TA_LEFT, fontSize=11, fontName="Calibri")
		p8 = ParagraphStyle('p8', alignment=TA_CENTER, fontSize=12, fontName="Calibri-Bold")
		p9 = ParagraphStyle('p9', alignment=TA_CENTER, fontSize=10, fontName="Calibri-Bold")
		p10 = ParagraphStyle('p10', alignment=TA_RIGHT, fontSize=10, fontName="Calibri")
		p11 = ParagraphStyle('p11', alignment=TA_CENTER, fontSize=11, fontName="Calibri-Bold")
		p12 = ParagraphStyle('p12', alignment=TA_CENTER, fontSize=10, fontName="Calibri")

		style = getSampleStyleSheet()["Normal"]
		gray = colors.Color(red=(225.0/255),green=(225.0/255),blue=(225.0/255))
		blue1 = colors.Color(red=(217.0/255),green=(226.0/255),blue=(243.0/255)) 
		blue2 = colors.Color(red=(26.0/255),green=(121.0/255),blue=(189.0/255)) 
		##
		white = colors.Color(red=(255/255),green=(255/255),blue=(255/255))
		black = colors.Color(red=(50/255),green=(50/255),blue=(50/255))
		red = colors.Color(red=(185/255),green=(18/255),blue=(18/255))
		##
		#
		p13 = ParagraphStyle('p13', alignment=TA_LEFT, fontSize=10, fontName="Calibri-Bold", textColor=white)
		p14 = ParagraphStyle('p14', alignment=TA_LEFT, fontSize=10, fontName="Calibri-Bold", textColor=red)
		p15 = ParagraphStyle('p15', alignment=TA_RIGHT, fontSize=10, fontName="Calibri",textColor=white)
		p16 = ParagraphStyle('p16', alignment=TA_CENTER, fontSize=14, fontName="Calibri-Bold", textColor=red)
		p17 = ParagraphStyle('p17', alignment=TA_LEFT, fontSize=10, fontName="Calibri-Bold",textColor=white)

		#
		months = {
			1: "Enero",
			2: "Febrero",
			3: "Marzo",
			4: "Abril",
			5: "Mayo",
			6: "Junio",
			7: "Julio",
			8: "Agosto",
			9: "Septiembre",
			10: "Octubre",
			11: "Noviembre",
			12: "Diciembre",
		}


		def header(c):
			com = self.env.company
			if com.logo:
				c.drawImage(ImageReader(get_encoded_image(com.logo)), pos_left, hPage-128, width=130, height=100, mask='auto')

		def header_table(c, pos):
			data=[
				[Paragraph("DESCRIPCIÓN", p6),Paragraph("CANTIDAD", p5), Paragraph("PRECIO UNITARIO", p5),Paragraph("DESC,%", p5),Paragraph("IMPUESTOS", p5), Paragraph("IMPORTE", p5)],
			]
			hTable=Table(data, colWidths=col_widths, rowHeights=(32))
			hTable.setStyle(TableStyle([
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('BOX',(0,1), (-1,-1),1, black),
				('TEXTCOLOR',(0, 1),(-1,-1), blue2),
				('SPAN', (0,0), (0,0)),
				#
				('GRID', (0,0), (-1,-1),0.5,black),
				#
				]))
			hTable.wrapOn(c, 120, 500)
			hTable.drawOn(c, pos_left, pos)

		c.drawImage(ImageReader(os.path.dirname(os.path.abspath(__file__))+'/img/Caratula.jpg'),0,0, width=wPage+0, height=hPage+0, mask='auto')
		c.showPage()
		header(c)


		for i, order in enumerate(self, 1):
			RUC_partner= order.partner_id.vat
			date_order = order.date_order
			validity_date = order.validity_date
			payment_term = order.payment_term_id.name
			user_name = order.user_id.name
			quo_name = order.name
			partner_name = order.partner_id.name
			RUC_company = order.company_id.vat
			partner_street = order.partner_id.street
			contact_order = order.contact_order_id.name
			amount_untaxed = order.amount_untaxed
			amount_tax = order.amount_tax
			amount_total = order.amount_total
			notes = order.note
			currency = order.currency_id.name

			if payment_term is False:
				payment_term = ' '
			if contact_order is False:
				contact_order = ' '

			c.setFont("Calibri", 10)
			c.drawString(360,770,u'RUC: '+str(RUC_company))
			c.drawString(360,760,u'Mz. l Lote 21 Int. 2Urb. Tahuaycani(Segundo piso)')
			c.drawString(360,750,u'AREQUIPA -- AREQUIPA - SACHACA')

			data = [
				['', Paragraph(u'Cotización # '+str(quo_name), p16)],
				['', ''],
				[Paragraph(u'Fecha cotización: '+str(date_order) or '',p7), Paragraph(u'Consultor de ventas: '+str(user_name) or '', p7)],
				['', ''],
				[Paragraph(u'Fecha vencimiento: '+str(validity_date) or '',p7), Paragraph(u'Plazo de pago: '+str(payment_term) or '', p7)],
				['', ''],
				['', ''],
				[Paragraph(u'Cliente: '+str(partner_name) or '' ,p7), ''],
				['', ''],
				[Paragraph(u'RUC/DNI: '+str(RUC_partner) or '', p7), ''],
				['', ''],
				[Paragraph(u'Dirección: '+str(partner_street) or '', p7), ''],
				['', ''],
				[Paragraph(u'Contacto: '+str(contact_order) or '', p7), ''],
			]
			t = Table(data, colWidths=[float(i) / 100 * wUtil for i in (50, 50)], rowHeights=(separator))
			t.setStyle(TableStyle([
				('VALIGN', (0, 0), (-1,-1), 'MIDDLE'),
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c, 120, 500)
			pos -= h_table
			t.drawOn(c,pos_left,pos)
			pos -= separator

		pos -= 65
		header_table(c, pos)

		for l in order.order_line:

			description = str(l.name)
			quantity = l.product_uom_qty
			price_unit = l.price_unit
			discount = l.discount
			tax = l.tax_id
			real_tax = ', '.join(tax.mapped('name'))
			price_subtotal = l.price_subtotal
			para_description = Paragraph(str(description.replace("\n","<br/>")) or '', p2)

			if currency == 'PEN':
				currency = 'S/. '
			if currency == 'USD':
				currency = "$ "

			if description in ['Servicios de Implementación','Personalizaciones','Servicios de soporte','Servidores','Licencias de Odoo','Facturación electrónica']:
				para_description = Paragraph(str(description.replace("\n","<br/>")) or '', p17)

			data = [
					[para_description, Paragraph(str(quantity) or '', p4), Paragraph(str(price_unit) or '', p4), 
						Paragraph(str(discount), p4),Paragraph(str(real_tax) or '', p4), Paragraph(str(currency)+str(price_subtotal) or '', p4)],
				]
			t2=Table(data, colWidths=col_widths)
			if description in ['Servicios de Implementación','Personalizaciones','Servicios de soporte','Servidores','Licencias de Odoo','Facturación electrónica']:
				t_style = [
					('VALIGN',(0,0),(-1,-1),'TOP'),
					('GRID', (0,0), (-1,-1),0.5,black),
					('SPAN', (0,0), (-1,-1)),
					('BACKGROUND',(0,0),(-1,-1),black),
				]
			else:
				t_style = [
					('VALIGN',(0,0),(-1,-1),'TOP'),
					('GRID', (0,0), (-1,-1),0.5,black),
				]

			t2.setStyle(TableStyle(t_style))
			w_table, h_table=t2.wrap(0,0)
			t2.wrapOn(c, 120, 500)
			pos -= h_table
			if pos < bottom:
				c.showPage()
				header(c)
				pos = hPage-180
				header_table(c, pos)
				pos-=h_table
				t2.drawOn(c,pos_left, pos)
			else: t2.drawOn(c, pos_left, pos)

		data = [
				['','','','',Paragraph('Subtotal',p14),Paragraph(str(currency)+str(amount_untaxed) or '',p4)],
				['','','','',Paragraph('IGV',p2),Paragraph(str(currency)+str(amount_tax) or '',p4)],
				['','','','',Paragraph('Total ',p13),Paragraph(str(currency)+str(amount_total) or '', p15)],
			]
		t3=Table(data, colWidths=col_widths)

		t_style = [
			('VALIGN',(0,0),(-1,-1),'TOP'),
			('BOX', (4,0), (-1,-1),0.5,black),
			('GRID', (-2,-2), (-1,-1),0.5,black),
			('BACKGROUND', (-1,-2), (-1,-1),gray),
			('BACKGROUND', (-2,-1), (-1,-1),red),
		]
		t3.setStyle(TableStyle(t_style))
		w_table, h_table=t3.wrap(0,0)
		t3.wrapOn(c, 120, 500)
		pos -= h_table
		if pos < bottom:
			c.showPage()
			header(c)
			pos = hPage-180
			header_table(c, pos)
			pos-=h_table
			t3.drawOn(c,pos_left, pos)
		else: t3.drawOn(c, pos_left, pos)


		data = [
			[Paragraph(str(notes.replace("\n","<br/>")),p3), ''],
		]
		t = Table(data, colWidths=595, rowHeights=(separator))
		t.setStyle(TableStyle([
			('VALIGN', (0, 0), (-1,-1), 'MIDDLE'),
		]))

		w_table,h_table=t.wrap(0,0)
		t.wrapOn(c, 120, 500)
		pos -= h_table
		t.drawOn(c,pos_left,pos-85)
		pos -= separator


		c.showPage()
		c.drawImage(ImageReader(os.path.dirname(os.path.abspath(__file__))+'/img/reverso.jpg'),0,0, width=wPage+0, height=hPage+0, mask='auto')
		c.showPage()
		c.setAuthor(self.env.company.name)
		c.setTitle(file_name)
		c.setSubject('Reportes')
		c.save()
		#Exportación
		return self.env['bo.report.base'].export_file(path, file_name)
