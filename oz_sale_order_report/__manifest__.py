# -*- encoding: utf-8 -*-
{
	'name': 'Orden de Venta Personalizada',
	'category': 'sale',
	'author': 'ITGRUPO-OZSOLUTIONS',
	'depends': ['sale_base_it','report_it','bo_report_base'],
	'version': '1.0',
	'description':"""
		Función: Personaliza el formato de Orden de Venta del Odoo.
		Requiere: Multimoneda, multialmacen, Descuentos.

	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'views/sale_order_view.xml',
		],
	'installable': True
}
